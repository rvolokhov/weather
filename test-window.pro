#-------------------------------------------------
#
# Project created by QtCreator 2015-02-06T17:12:10
#
#-------------------------------------------------

QT       += core gui xml

QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test-window
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

