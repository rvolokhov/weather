#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QCoreApplication>
#include <QDebug>
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QXmlStreamReader>
#include <QFile>
#include <QTimer>

QVector< QVector<QString> > mainVector;
QTimer *timer;
int mainIndex = 0;//index of output vector

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
        ui->setupUi(this);
        timer = new QTimer(this);
        QObject::connect(timer, SIGNAL(timeout()), this, SLOT(begin()));
        timer->setSingleShot(false);
        timer->start(1000);
        begin();
    }

MainWindow::~MainWindow()
{
    delete ui;
}

//function update every minute
void MainWindow::begin(){
    mainVector.clear();
    getData(QString("http://informer.gismeteo.ru/rss/27113.xml"));//Cherepovets
    getData(QString("http://informer.gismeteo.ru/rss/27612.xml"));//Moscow
}

//get data from specified url and parse it
void MainWindow::getData(QString link){
    QString xmlData;
    sendRequest(&xmlData, link);
    //xml is in xmlData
    QXmlStreamReader xml(xmlData);
    QVector<QString> weatherVector = parseXML(&xmlData).toVector();
    mainVector.append(weatherVector);
    printData(mainIndex);
}

//get xml
void MainWindow::sendRequest(QString *xmlData, QString link){
    // create custom temporary event loop on stack
    QEventLoop eventLoop;
    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    // the HTTP request
    QUrl url = link;
    QNetworkRequest req( url );
    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec(); // blocks stack until "finished()" has been called

    if (reply->error() == QNetworkReply::NoError) {
        *xmlData = reply->readAll();
    }
    else {
        *xmlData = "Failure";
    }
    delete reply;
}

QList<QString> MainWindow::parseXML(QString *xmlData){
    QXmlStreamReader xml;
    QList<QString> xmlStr;
    xml.addData(*xmlData);
    while (!xml.atEnd()) {
        if(xml.isStartElement()){
            if(xml.name() == "title"){
                xml.readNext();
                xmlStr.append(xml.text().toString());
            }
            if(xml.name() == "description"){
                xml.readNext();
                xmlStr.append(xml.text().toString());
            }
        };
        xml.readNext();
    };

    return xmlStr;
}

//output data on form
void  MainWindow::printData(int index){
    //cut excess part and output in label
    QVector<QString> tmp = mainVector.value(index);
    ui->label_2->setText(tmp.value(3).mid(0, tmp.value(3).indexOf(":") ));
    ui->label_3->setText(tmp.value(3).mid( tmp.value(3).indexOf(":")+1 ));
    ui->label_4->setText(tmp.value(4).mid(0, tmp.value(4).indexOf("давление")-2));
    ui->label_5->setText(tmp.value(5).mid(tmp.value(5).indexOf(":")+1));
    ui->label_6->setText(tmp.value(6).mid(0, tmp.value(6).indexOf("давление")-2));
    ui->label_7->setText(tmp.value(7).mid(tmp.value(7).indexOf(":")+1));
    ui->label_8->setText(tmp.value(8).mid(0, tmp.value(8).indexOf("давление")-2));
    ui->label_9->setText(tmp.value(9).mid(tmp.value(9).indexOf(":")+1));
    ui->label_10->setText(tmp.value(10).mid(0, tmp.value(10).indexOf("давление")-2));
}

void MainWindow::on_pushButton_4_clicked(){
    if(mainIndex<mainVector.count()-1)
        mainIndex++;
    printData(mainIndex);
}

void MainWindow::on_pushButton_3_clicked(){
    if(mainIndex>0)
        mainIndex--;
    printData(mainIndex);
}
